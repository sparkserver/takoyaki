use std::{
    fs::{self, File},
    io::{BufReader, BufWriter, Read, Write},
    path::Path,
};

use argh::FromArgs;
use binread::BinRead;
use binwrite::BinWrite;
use byteorder::{ByteOrder, LE};

#[derive(FromArgs)]
/// Replaces grain table in a GIN file
struct Args {
    /// input gin file
    #[argh(option, short = 'i')]
    pub in_file: String,

    /// output gin file
    #[argh(option, short = 'o')]
    pub out_file: String,

    /// table file generated by gingtgen
    #[argh(option, short = 't')]
    pub table_file: String,
}

#[derive(BinRead, BinWrite, Debug)]
#[br(little, magic = b"Gnsu20\x00\x00")]
#[binwrite(little)]
pub struct GinFile {
    pub start_rpm: f32,
    pub end_rpm: f32,
    pub table_1_count: u32,
    pub table_2_count: u32,
    pub sample_count: u32,
    pub sample_rate: u32,
    #[br(count = table_1_count+1)]
    pub table_1: Vec<u32>,
    #[br(count = table_2_count+1)]
    pub table_2: Vec<u32>,
}

fn read_table<P: AsRef<Path>>(path: P) -> std::io::Result<Vec<u32>> {
    let bytes = fs::read(path)?;
    assert_eq!(bytes.len() % 4, 0);

    let mut table = vec![0u32; bytes.len() / 4];
    for i in 0..(bytes.len() / 4) {
        table[i] = LE::read_u32(&bytes[i * 4..i * 4 + 4]);
    }
    Ok(table)
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args: Args = argh::from_env();

    let table = read_table(&args.table_file)?;
    let real_sample_count = table.last().copied().expect("empty table");

    let mut in_file = BufReader::new(File::open(&args.in_file)?);
    let mut header = GinFile::read(&mut in_file)?;
    header.table_2_count = (table.len() as u32) - 1;
    header.table_2 = table;
    if header.sample_count != real_sample_count {
        println!(
            "Correcting sample count from {} to {}",
            header.sample_count, real_sample_count
        );
        header.sample_count = real_sample_count;
    }

    let mut data_buf: Vec<u8> = Vec::new();
    in_file.read_to_end(&mut data_buf)?;

    let mut out_file = BufWriter::new(File::create(&args.out_file)?);
    out_file.write_all(b"Gnsu20\x00\x00")?;
    header.write(&mut out_file)?;
    out_file.write_all(&data_buf)?;
    out_file.flush()?;

    Ok(())
}
