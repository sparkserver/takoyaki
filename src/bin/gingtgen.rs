use std::{
    ffi::OsStr,
    fs::{self, File},
    io::{BufWriter, Write},
    path::Path,
};

use argh::FromArgs;
use byteorder::{WriteBytesExt, LE};
use hound::{SampleFormat, WavSpec};

#[derive(FromArgs)]
/// Generate .gin grain table from .wav grains
struct Args {
    /// directory with .wav files representing the grains
    #[argh(option, short = 'd')]
    pub grain_dir: String,

    /// file to write the grain table to
    #[argh(option, short = 'o')]
    pub out_file: String,

    /// generate WAV file with all grains
    #[argh(option, short = 'w')]
    pub wav_file: Option<String>,
}

struct GrainWriter {
    grains: Vec<u32>,
    samples: Vec<i16>,
    sample_rate: Option<u32>,
}

impl GrainWriter {
    pub fn count(&self) -> u32 {
        self.grains.len() as u32
    }

    pub fn last_grain_end(&self) -> u32 {
        self.grains.last().copied().unwrap_or(0)
    }

    pub fn sample_count(&self) -> u32 {
        self.samples.len() as u32
    }

    pub fn add_grain_with_duration(&mut self, duration: u32) {
        if self.grains.len() == 0 {
            self.grains.push(0);
            self.grains.push(duration);
        } else {
            self.grains
                .push(duration + self.grains[self.grains.len() - 1])
        }
    }

    pub fn add_samples(&mut self, samples: &[i16]) {
        self.samples.extend_from_slice(samples);
    }

    pub fn set_sample_rate(&mut self, rate: u32) {
        if self.sample_rate.is_none() {
            self.sample_rate = Some(rate);
            println!("Sample rate set to {}", rate);
        }
    }

    pub fn write_to_file<P: AsRef<Path>>(&self, path: P) -> std::io::Result<()> {
        let mut file = BufWriter::new(File::create(path)?);

        for grain in self.grains.iter() {
            file.write_u32::<LE>(*grain)?;
        }

        file.flush()
    }

    pub fn write_wav_file<P: AsRef<Path>>(
        &self,
        path: P,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let mut file = hound::WavWriter::create(
            path,
            WavSpec {
                channels: 1,
                sample_rate: self.sample_rate.unwrap(),
                bits_per_sample: 16,
                sample_format: SampleFormat::Int,
            },
        )?;

        let mut padding = 32 - (self.sample_count() % 32);
        if padding == 32 {
            padding = 0;
        }
        if padding > 0 {
            println!(
                "Padding WAV with {} samples to work around gin_encode bug",
                padding
            );
        }

        let mut wr = file.get_i16_writer(self.sample_count() + padding);
        for sample in self.samples.iter() {
            wr.write_sample(*sample);
        }
        for _ in 0..padding {
            wr.write_sample(0);
        }
        wr.flush()?;
        file.finalize()?;

        Ok(())
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args: Args = argh::from_env();

    let mut grains = GrainWriter {
        grains: Vec::new(),
        samples: Vec::new(),
        sample_rate: None,
    };
    let read_dir = fs::read_dir(&args.grain_dir)?;
    for entry in read_dir {
        let entry = entry?;
        if let Some(ext) = entry.path().extension() {
            if ext == OsStr::new("wav") {
                handle_file(entry.path(), &mut grains)?;
            }
        }
    }

    println!();

    assert_eq!(grains.sample_count(), grains.last_grain_end());

    grains.write_to_file(args.out_file)?;

    if let Some(wav_path) = args.wav_file {
        grains.write_wav_file(&wav_path)?;
    }

    let count = grains.count();
    println!("Total samples: {}", grains.sample_count());
    println!("{} grains written to output file", count);
    /*println!(
        "In your .gin file, patch 4 bytes at offset +0x14 to this: {:02X} {:02X} {:02X} {:02X}",
        count & 0xFF,
        (count >> 8) & 0xFF,
        (count >> 16) & 0xFF,
        (count >> 24) & 0xFF
    );*/

    Ok(())
}

fn handle_file<P: AsRef<Path>>(
    path: P,
    grains: &mut GrainWriter,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut rd = hound::WavReader::open(path.as_ref())?;

    let spec = rd.spec();
    if spec.sample_format != SampleFormat::Int || spec.bits_per_sample != 16 {
        return Err("The WAV file must be in signed 16-bit PCM format".into());
    }
    if spec.channels != 1 {
        return Err("The WAV file must be mono (have only one channel)".into());
    }

    println!(
        "{:?} - {} samples",
        path.as_ref()
            .file_name()
            .unwrap_or(OsStr::new("Unknown filename")),
        rd.duration()
    );

    let samples = rd.samples().collect::<Result<Vec<i16>, _>>()?;

    grains.set_sample_rate(spec.sample_rate);
    grains.add_grain_with_duration(rd.duration());
    grains.add_samples(&samples);

    Ok(())
}
